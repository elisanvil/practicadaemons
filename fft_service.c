#include "csapp.h"
#include "fft.h"
#include "commands.h"


#include "apue.h"
#include <syslog.h>
#include <fcntl.h>
#include <sys/resource.h>


const size_t S_DATA = sizeof(complex)*N;

enum Estado {EXIT = 0, WAITING_COMMAND, WAITING_DATA, DOING_FFT, SENDING_DATA, DISCONNECTING} estado; //Estados para la FSM

void responder_cliente(int connfd);

int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;
    char *cmd;

    if ((cmd = strrchr(argv[0], '/')) == NULL)
		cmd = argv[0];
	else
		cmd++;
    
	//Become a daemon
	daemonize(cmd);

	//Make sure only one copy of the daemon is running.
	if (already_running()) {
		syslog(LOG_ERR, "daemon already running");
		exit(1);
	}    

    openlog("lpd", LOG_PID, LOG_LPR);



	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	estado = DISCONNECTING;
	listenfd = Open_listenfd(port);
	while (estado) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);

		responder_cliente(connfd);
		//printf("desconectando al cliente ...\n");
                
        syslog (LOG_INFO, "desconectando al cliente ...\n");
		Close(connfd);
	}
	//printf("Bye...\n");
    syslog (LOG_INFO, "Bye...\n");
    closelog ();	
    exit(0);
}

void responder_cliente(int connfd)
{
	char buf[MAXLINE];
	complex data[N];
	complex data_tmp[N];
	size_t n;
	rio_t rio;

	estado = WAITING_COMMAND;
	Rio_readinitb(&rio, connfd);

    openlog("lpd", LOG_PID, LOG_LPR);

	while(estado) {
		switch(estado)
		{
			case WAITING_COMMAND:
				n = Rio_readlineb(&rio, buf, MAXLINE);

				if(n>0)
				{
					if(strcmp(buf,STOP) == 0){
						Rio_writen(connfd, QUIT, 5);
						estado = EXIT;
					} else if (strcmp(buf,DISCONNECT) == 0){
						estado = DISCONNECTING;
					} else if (strcmp(buf,FFT) == 0){
						estado = WAITING_DATA;
					} else {
						Rio_writen(connfd, UNKNOWN, 3);
						//printf("Comando desconocido\n");
                        syslog (LOG_ERR, "Comando desconocido\n");
					}
				}else
					estado = DISCONNECTING;
				break;
			case WAITING_DATA:
				Rio_writen(connfd, OK, 3);
				//printf("Recibiendo trama de %d puntos\n",N);
                syslog (LOG_INFO, "Recibiendo trama de %d puntos\n",N);
				size_t n = Rio_readn(connfd, (void *)data, S_DATA);
				if(n == S_DATA)
					estado = DOING_FFT;
				else {
					//printf("Error recibiendo datos, desconectando...");
                    syslog (LOG_ERR, "Error recibiendo datos, desconectando...");
					estado = DISCONNECTING;
				}
				break;
			case DOING_FFT:
				fft(data,N,data_tmp);
				estado = SENDING_DATA;
				break;
			case SENDING_DATA:
				Rio_writen(connfd, (void *) data, S_DATA);
				estado = WAITING_COMMAND;
				break;
			case DISCONNECTING:
				Rio_writen(connfd, BYE, 4);
				return;
				break;
			default:
				estado = DISCONNECTING;
		}
	}
    closelog ();
}


void
daemonize(const char *cmd)
{
    int i, fd0, fd1, fd2;
    pid_t pid;
    struct rlimit rl;
    struct sigaction sa;

    /*
     * Clear file creation mask.
     */
    umask(0);

    /*
     * Get maximum number of file descriptors.
     */
    if (getrlimit(RLIMIT_NOFILE, &rl) < 0)
        err_quit("%s: can't get file limit", cmd);

    /*
     * Become a session leader to lose controlling TTY.
     */
    if ((pid = fork()) < 0)
        err_quit("%s: can't fork", cmd);
    else if (pid != 0) /* parent */
        exit(0);
    setsid();

    /*
     * Change the current working directory to the root so
     * we won't prevent file systems from being unmounted.
     */
    if (chdir("/") < 0)
        err_quit("%s: can't change directory to /", cmd);

    /*
     * Close all open file descriptors.
     */
    if (rl.rlim_max == RLIM_INFINITY)
        rl.rlim_max = 1024;
    for (i = 0; i < rl.rlim_max; i++)
        close(i);

    /*
     * Attach file descriptors 0, 1, and 2 to /dev/null.
     */
    fd0 = open("/dev/null", O_RDWR);
    fd1 = dup(0);
    fd2 = dup(0);

    /*
     * Initialize the log file.
     */
    openlog(cmd, LOG_CONS, LOG_DAEMON);
    if (fd0 != 0 || fd1 != 1 || fd2 != 2) {
        syslog(LOG_ERR, "unexpected file descriptors %d %d %d",
          fd0, fd1, fd2);
        exit(1);
    }
}
